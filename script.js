

$("#datepicker").datepicker({
  showOtherMonths: 1,
  changeMonth: 0,
  changeYear: 0,
  dateFormat: "dd.mm.yy",

  language: 'es',
  locale: 'es',
  startDate: "now",
  minDate: 0

});

$(".date").mousedown(function() {
  $(".ui-datepicker").addClass("active");
});



$.datepicker.regional['es'] = {

  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
  monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
  dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
  dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
  weekHeader: 'Sm',
  dateFormat: 'dd/mm/yy',
  firstDay: 1,
  isRTL: false,
  showMonthAfterYear: false,
  yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['es']);
 $(function () {
 $("#datepicker").datepicker();
 });